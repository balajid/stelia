var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var neo4j = require('neo4j-driver').v1;
var app = express();
var xlsx = require('xlsx');
var busboy = require('connect-busboy');
var json2xls = require('json2xls');
var json2csv = require('json2csv').parse;
var newLine="\r\n";
const fileUpload = require('express-fileupload');
const fs = require('fs');
const arr=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
var filename="";
var file2="";
var error=true;
var msg="";
var threshold=0;
//view engine
var os = require("os");
app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');

app.use(busboy());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname,'public')));
app.use(fileUpload());

var driver = neo4j.driver('bolt://localhost',neo4j.auth.basic('neo4j','oracle'));
var session = driver.session();
app.listen(3000);
console.log('Server started');

app.get('/',function(req,res){

     res.render('index');
});
app.get('/node/report',function(req,res){
      console.log('in');
      var workbook3=xlsx.readFile(file2+'.xlsx');
      var sheet_name_list = workbook3.SheetNames;
      jsa=xlsx.utils.sheet_to_json(workbook3.Sheets[sheet_name_list[0]])
      //console.log(jsa[0])

      var keys3=[]
      for(i in jsa[0]){
        //console.log(i,jsa[0][i])
        keys3.push(i)
      }

      var toCsv = {  keys3 };


      var csv = json2csv(jsa,toCsv) + newLine;
      console.log(error);
      if(error==false)
      {
        fs.appendFile(filename+".csv", "Results:  PASS"+os.EOL+os.EOL, (err) => {

            if (err) throw err;

            console.log('pass');
        });
      }
      else{

        fs.appendFile(filename+".csv", "Results:  FAIL    "+msg+"   "+os.EOL+os.EOL, (err) => {

            if (err) throw err;

            console.log('fail');
        });

      }
      fs.appendFile(filename+".csv",csv,function(err){
          if (err) throw err;
          console.log("the data was appended");
          res.download(filename+".csv");
      });

});
app.get('/generate',function(req,res){

      res.render('generate');

});
app.post('/node/add',function(req,res){
    const rampdate=req.body.date1;
    const time1=req.body.time1;
    const time2=req.body.time2;
    var texter="";
    const gs=rampdate.split("-");
    const date=gs[1]+"/"+gs[2]+"/"+gs[0];
    console.log(date);
    console.log(time1);
    console.log(time2);
    const time3=time2.split(":");
    const mm=(parseInt(time3[1])+10)%60;
    var hh=parseInt(time3[0]);
    if((parseInt(time3[1])+10)>60)
        hh=(parseInt(time3[0])+1)%24;
    var time4=hh+":"+mm+":"+time3[2];
    var time5=hh+":"+mm+":"+(parseInt(time3[2])+1)
    console.log(time4);
    console.log(time5);
    const mon=arr[gs[1]-1]+gs[0];
    filename=mon;
    let content='The date is '+date+'and the time is '+time1+'\n';
    const query="match(n:equipment{name:'autoclave'}),(b:equipment{name:'ControlTC'}) match(n)<-[r:monitors]-(b) where r.date=apoc.date.parse('"+date+"','s','mm/dd/yyyy') and r.time=apoc.date.parse('"+time1+"','s','hh:mm:ss') return r.value";
    const query2="match(n:equipment{name:'autoclave'}),(b:equipment{name:'ControlTC'}) match(n)<-[r:monitors]-(b) where r.date=apoc.date.parse('"+date+"','s','mm/dd/yyyy') and r.time=apoc.date.parse('"+time2+"','s','hh:mm:ss') return r.value";
    const query3="match(n:equipment{name:'autoclave'}),(b:equipment) match(n)<-[r:monitors]-(b) where r.date=apoc.date.parse('"+date+"','s','mm/dd/yyyy') and r.time=apoc.date.parse('"+time4+"','s','hh:mm:ss') or r.time=apoc.date.parse('"+time5+"','s','hh:mm:ss') return avg(toInt(r.value)),max(toInt(r.value)),min(toInt(r.value))";
    session.run(query)
           .then(function(result){
                result.records.forEach(function(record){
                    //console.log(record._fields[0]);
                    var texter="Temperature of Controlling Tc at ramp start time at  "+time1;
                    const avg=record._fields[0];
                    texter+=" :  "+avg+os.EOL;
                    if(!fs.existsSync(filename+".csv"))
                    {
                          fs.writeFileSync(filename+".csv",texter+os.EOL,'binary');
                        /*fs.writeFile(filename+".csv",texter+os.EOL,(err) => {

                             if (err) throw err;

                             console.log('content saved!');
                        });*/
                    }
                    else {
                        fs.appendFile(filename+".csv", texter+os.EOL, (err) => {

                            if (err) throw err;

                            console.log('The content were updated!');
                        });
                    }
                    //res.download('/gre.pdf');
                });
           })
           .catch(function(err){
                console.log(err);
           });
     session.run(query2)
            .then(function(result){
                  result.records.forEach(function(record){
                     //console.log(record._fields[0]);
                     var texter2="Temperature of Controlling TC at Dwell start time at  "+time2;
                     const avg1=record._fields[0];
                     texter2+=" :  "+avg1+os.EOL;
                     const temp=parseInt(avg1);

                     if(avg1>250)
                     {
                         threshold=260;
                     }
                     if(avg1>345)
                     {
                       threshold=355;
                     }
                     if(avg1>390)
                     {
                        threshold=400;
                     }

                     //res.download('/gre.pdf');
                     if(!fs.existsSync(filename+".csv"))
                     {
                          fs.writeFileSync(filename+".csv",texter2,'binary');
                     }
                     else {
                         fs.appendFile(filename+".csv", texter2+os.EOL, (err) => {

                             if (err) throw err;

                             console.log('The content were updated!');
                         });
                     }
                 });
            })
            .catch(function(err){
                 console.log(err);
            });
      session.run(query3)
             .then(function(result){
                   result.records.forEach(function(record){
                      //console.log(record._fields[0]);
                      const avg2=record._fields[0];
                      var texter3="Temperature of Leading TC after 10 minutes at Dwell at time  "+time4;
                      const min2=(record._fields[1]!=null)?record._fields[1].low:null;
                      const max2=(record._fields[2]!=null)?record._fields[2].low:null;
                      if(min2!=null && max2!=null)
                      {
                          if(parseInt(min2)>=(threshold-10) && parseInt(min2)<=(threshold+10))
                          {
                              if(parseInt(max2)>=(threshold-10) && parseInt(max2)<=(threshold+10))
                              {
                                 error=false;
                              }
                              else {
                                msg+="The temperature drift above limit during"+parseInt(threshold)+"dwell";
                              }
                          }
                          else {
                              msg+="The temperature drift above limit during"+parseInt(threshold)+"dwell";
                          }
                      }
                      texter3+=" :  "+max2+os.EOL+os.EOL;
                      texter3+="Temperature of Lagging TC after 10 minutes at Dwell at time  "+time4;
                      texter3+=" :  "+min2+os.EOL+os.EOL;
                      console.log(min2,max2);
                      console.log(texter3);
                      if(!fs.existsSync(filename+".csv"))
                      {
                            fs.writeFileSync(filename+".csv",texter3,'binary');
                      }
                      else {
                          fs.appendFile(filename+".csv", texter3+os.EOL, (err) => {

                              if (err) throw err;

                              console.log('The content were updated!');
                          });
                      }
                      //res.download('/gre.pdf');
                  });
             })
             .catch(function(err){
                  console.log(err);
             });
             file2=arr[gs[1]-1]+(parseInt(gs[0])-2000);

             res.redirect('/');

});
app.post('/node/upload',function(req,res){

      if(!req.files)
          return res.status(400).send('no files were uploaded');

        let generateFile = req.files.GenerateFile;

        //console.log(generateFile);
        console.log(__dirname+'/files/'+generateFile.name+'');
        /*generateFile.mv(__dirname+'/'+generateFile.name+'',function(err){
            if(err)
              return res.status(500).send(err);

            console.log('file uploading'+generateFile.name);
        });*/
        generateFile.mv('C://Users/superuser/.Neo4jDesktop/neo4jDatabases/database-a602f578-b9ec-4880-b7bb-57cd0ff5aa73/installation-3.3.5/import/stelia'+'/'+generateFile.name+'',function(err){
            if(err)
                return res.status(500).send(err);

            console.log('File moved to Database section');

        });
        console.log(generateFile.name);
        var workbook=xlsx.readFile(generateFile.name);
        var sheet_name_list = workbook.SheetNames;
        jsa1=xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]])
        console.log(jsa1[0])
        var keys1=[]
        for(i in jsa1[0]){
           //console.log(i,jsa[0][i])
           keys1.push(i)
        }
        console.log(keys1);
        var toCsv = {  keys1 };
        var name=generateFile.name.split(".");
        console.log(name[0]);
        var csv = json2csv(jsa1,toCsv) + newLine;

        fs.writeFile('C://Users/superuser/.Neo4jDesktop/neo4jDatabases/database-a602f578-b9ec-4880-b7bb-57cd0ff5aa73/installation-3.3.5/import/stelia'+'/'+name[0]+'.csv',csv,function(err){
            if (err) throw err;
              console.log("File Created");

        });
        var que="";
        var k=0;
        for(i=0;i<keys1.length;i++){
            if(keys1[i]=="Date" || keys1[i]=="Time")
            {
              if(k==0){
                que='create(n:equipment{name:"autoclave"}) return n';
                k++;
              session.run(que)
                     .then(function(result){
                          result.records.forEach(function(record){
                              console.log(record);
                          });
                     })
                     .catch(function(err){
                          console.log(err);
                     });
               }
              continue;
            }
            que='create(n:equipment{name:"'+keys1[i]+'"}) return n';
            session.run(que)
                   .then(function(result){
                        result.records.forEach(function(record){
                            console.log(record);
                        });
                   })
                   .catch(function(err){
                        console.log(err);
                   });
            que2='load csv with headers from "'+"file:///stelia//"+name[0]+".csv"+'" as line match (n:equipment{name:"autoclave"}),(b:equipment{name:"'+keys1[i]+'"}) merge (n)<-[r:monitors{name:"'+keys1[i]+'",value:line.'+keys1[i]+', date:apoc.date.parse(line.Date,"'+"s"+'","'+"mm/dd/yyyy"+'"),time:apoc.date.parse(line.Time,"'+"s"+'","'+"hh:mm:ss"+'")}]-(b) return n,b';
            session.run(que2)
                   .then(function(result){
                        result.records.forEach(function(record){
                            console.log(record);
                        });
                   })
                   .catch(function(err){
                        console.log(err);
                   });

        }
        res.redirect("/");
});
module.exports = app;
